package main

import (
	"database/sql"
	"flag"
	"fmt"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/magiconair/properties"
	"os"
	"time"
)

type AppConfig struct {
	host     string
	port     int
	user     string
	password string
	dbname   string
	timeout  int
	query    string
}

var pidInfo map[int]time.Time

func usage() {
	fmt.Fprintf(os.Stderr, "usage: example -stderrthreshold=[INFO|WARN|FATAL] -log_dir=[string]\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func init() {
	flag.Usage = usage
	flag.Parse()
}

func getPidLocks(db *sql.DB, query string) (pidsInfo map[int32]time.Time) {

	var m = make(map[int32]time.Time)
	rows, err := db.Query(query)

	if err != nil {
		panic(err)
	}

	for rows.Next() {
		var pid int32
		var lockTime time.Time
		err = rows.Scan(&pid, &lockTime)

		if err != nil {
			panic(err)
		}
		m[pid] = lockTime
	}

	return m
}

func checkPidsInfoEmpty(pidsInfo map[int32]time.Time) bool {
	if len(pidsInfo) == 0 {
		return true
	} else {
		return false
	}
}

func getDbConnection(pgSqlInfo string) *sql.DB {
	db, err := sql.Open("postgres", pgSqlInfo)
	if err != nil {
		panic(err)
	}
	return db
}

func loadPropertiesConfig(configPath string) AppConfig {
	p := properties.MustLoadFile(configPath, properties.UTF8)
	host := p.MustGetString("host")
	port := p.MustGetInt("port")
	user := p.MustGetString("user")
	password := p.MustGetString("password")
	dbname := p.MustGetString("dbname")
	timeout := p.MustGetInt("timeout")
	query := p.MustGetString("query")

	return AppConfig{host, port, user, password, dbname, timeout, query}
}

func getConnectionString(config AppConfig) string {
	pgSqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.host, config.port, config.user, config.password, config.dbname)
	return pgSqlInfo
}

func closeDbConnection(db *sql.DB) {
	_ = db.Close()
}

func checkPidsInfoByTimeout(pidsInfo map[int32]time.Time) {
	now := time.Now()

	for pid, lockDate := range pidsInfo {
		diff := now.Sub(lockDate)
		fmt.Println("Time and diff", lockDate.String(), diff)
	}
}

func main() {

	configuration := loadPropertiesConfig("config.properties")
	db := getDbConnection(getConnectionString(configuration))
	pidsInfo := getPidLocks(db, configuration.query)
	closeDbConnection(db)

	if !checkPidsInfoEmpty(pidsInfo) {
		checkPidsInfoByTimeout(pidsInfo)
		glog.Info(time.Now(), "locks enabled!")
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}
